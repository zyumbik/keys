//
//  KeyView.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 07/06/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Cocoa

class KeyView: NSView {

    @IBOutlet var customView: NSView!
    
    var value: String = "a"
    var loadedFont: CTFont!
    var bgImage: NSImage!
    var isActive: Bool?
    
    init(value: String, font: CTFont, image: NSImage, isActive: Bool) {
        self.value = value
        self.loadedFont = font
        self.bgImage = image
        self.isActive = isActive
        
        super.init(frame: NSRect(x: 0, y: 0, width: 64, height: 64))
        setup()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        setup()
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        let context = NSGraphicsContext.current?.cgContext
//        if (isActive!) {
//            // pink if key is active
//            context!.setFillColor(red: 1, green: 0.325, blue: 0.525, alpha: 0.69)
//        } else {
//            // gray if key is inactive
//            context!.setFillColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.69)
//        }
        context!.setFillColor(red: 1, green: 0.325, blue: 0.525, alpha: 0.69)
        context!.setStrokeColor(red: 1, green: 1, blue: 1, alpha: 0)
        let rect = CGRect(x: 0, y: 0, width: 64, height: 64)
        let path = CGPath(roundedRect: rect, cornerWidth: 16, cornerHeight: 16, transform: nil)
        context!.addPath(path)
        context!.fillPath()
        
//        bgImage.draw(in: rect)
        
        // text properties
        context!.setAllowsFontSmoothing(false)
        let textColor = NSColor(red: 1, green: 1, blue: 1, alpha: 1)
        let textColorB = NSColor(red: 1, green: 1, blue: 1, alpha: 0.0)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let attr: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.paragraphStyle: paragraphStyle,
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.backgroundColor: textColorB,
            NSAttributedString.Key.font: loadedFont
        ]
        // text size and position
        let q: NSString = NSString(string: value)
        let targetSize: CGSize = CGSize(width: 64, height: 64)
        let boundingRect = q.boundingRect(with: targetSize, options: [.usesLineFragmentOrigin], attributes: attr)
        let origin = CGPoint(x: (64 - boundingRect.width) / 2 + 1, y: (64 - boundingRect.height) / 2 + 1)
        let textRect = CGRect(origin: origin, size: boundingRect.size)
        q.draw(in: textRect, withAttributes: attr)
    }
    
    private func setup() {
        let bundle = Bundle(for: type(of: self))
        let nib = NSNib(nibNamed: .init(String(describing: type(of: self))), bundle: bundle)!
        nib.instantiate(withOwner: self, topLevelObjects: nil)
        
        addSubview(customView)
    }
    
}
