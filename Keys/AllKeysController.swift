//
//  AllKeysController.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 19/05/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Cocoa

protocol KeysControllerDelegate {
    func showKeys(all: [KeysCombination])
}

class AllKeysController {
    
    fileprivate var combination: KeysCombination? = nil
    fileprivate var history: [KeysCombination] = []
    
    fileprivate var keysControllerDelegate: KeysControllerDelegate?
    
    func setDelegate(_ delegate: KeysControllerDelegate) {
        self.keysControllerDelegate = delegate
        self.combination = KeysCombination(delegate: self)
    }
    
    func onKeyDown(event: NSEvent) {
        guard !event.isARepeat else {
            return
        }
        let val = event.characters!
        let code = event.keyCode
        if (combination == nil) {
            combination = KeysCombination(delegate: self)
        }
        combination?.addKey(value: val, keyCode: code, flag: false)
    }
    
    func onKeyUp(event: NSEvent) {
        let code = event.keyCode
        self.combination?.removeKey(value: event.characters!, keyCode: code, flag: false)
    }
    
    private func onFlagChange(_ isActive: Bool, value: String, keyCode: UInt16) {
        if isActive {
            if (combination == nil) {
                combination = KeysCombination(delegate: self)
            }
            combination?.addKey(value: value, keyCode: keyCode, flag: true)
        } else {
            combination?.removeKey(value: value, keyCode: keyCode, flag: true)
        }
    }
    
    func onFlagsChange(event: NSEvent) {
        let flags = event.modifierFlags.intersection(.deviceIndependentFlagsMask)
        onFlagChange(flags.contains(.shift), value: "⇧", keyCode: event.keyCode)
        onFlagChange(flags.contains(.command), value: "⌘", keyCode: event.keyCode)
        onFlagChange(flags.contains(.control), value: "⌃", keyCode: event.keyCode)
        onFlagChange(flags.contains(.option), value: "⌥", keyCode: event.keyCode)
        onFlagChange(flags.contains(.capsLock), value: "⇪", keyCode: event.keyCode)
    }
    
}

extension AllKeysController: KeysCombinationDelegate {
    func update() {
        var rowsToShow = history
        if (combination != nil) {
            rowsToShow += [combination!]
        }
        keysControllerDelegate?.showKeys(all: rowsToShow)
    }
    
    func pushToHistory(stillPressed: [PressedKey]) {
        guard combination != nil else {
            return
        }
        history.append(combination!)
        history = Array(history.suffix(4))
        if (stillPressed.count > 0) {
            combination = KeysCombination(delegate: self, flags: stillPressed)
        } else {
            combination = nil
        }
        
        delay(2) {
            if (self.history.count > 0) {
                self.history.removeFirst()
                self.update()
            }
        }
    }
}
