//
//  PressedKey.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 07/06/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Foundation

class PressedKey {
    let value: String
    let keyCode: UInt16
    
    var isLongPressed = false
    var isPressed = true
    
    init(value: String, keyCode: UInt16) {
        self.keyCode = keyCode
        if let special = Special().keys[Int(keyCode)] {
            self.value = special
        } else if (value.isEmpty || !isInAlphabet(value.first!)),
            let custom = Special().letters[Int(keyCode)] {
            // not a valid string? Get the literal key value if possible
            self.value = custom
        } else {
            // not in the alphabet or is valid string
            self.value = value
        }
        
        delay(1) {
            self.isLongPressed = true
        }
    }
}

extension PressedKey: CustomStringConvertible {
    public var description: String {
        return "value: \(self.value) key: \(self.keyCode)"
    }
}

fileprivate struct Special {
    fileprivate let keys = [
        36: "⏎",
        53: "⎋",
        51: "⌫",
        117: "⌦",
        48: "⇥",
        121: "⇟",
        116: "⇞",
        115: "↖︎",
        119: "↘︎",
        49: "␣",
        126: "↑",
        125: "↓",
        124: "→",
        123: "←"
    ]
    fileprivate var letters = [
        12: "Q",
        13: "W",
        14: "E",
        15: "R",
        17: "T",
        16: "Y",
        32: "U",
        34: "I",
        31: "O",
        35: "P",
        0: "A",
        1: "S",
        2: "D",
        3: "F",
        5: "G",
        4: "H",
        38: "J",
        40: "K",
        37: "L",
        6: "Z",
        7: "X",
        8: "C",
        9: "V",
        11: "B",
        45: "N",
        46: "M",
    ]
}
