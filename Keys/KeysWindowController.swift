//
//  KeysWindowController.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 19/05/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Cocoa

class KeysWindowController: NSWindowController, NSWindowDelegate {
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(windowWillClose), name: NSWindow.willCloseNotification, object: nil)
        if let frame = storedFrame {
            window?.setFrame(frame, display: true)
        }
        
        window?.isOpaque = false
        window?.isMovableByWindowBackground = true
        window?.styleMask = [.borderless]
        window?.isRestorable = true
        window?.level = .floating
        window?.backgroundColor = .clear //NSColor(calibratedRed: 0.2, green: 0.4, blue: 0.1, alpha: 0.2)
        window?.collectionBehavior = [.canJoinAllSpaces]
    }

    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if let frame = storedFrame {
            window?.setFrame(frame, display: true)
        }
    }
    
    open override var contentViewController: NSViewController? {
        didSet {
            if let frame = storedFrame {
                window?.setFrame(frame, display: true)
            }
        }
    }
    
    // MARK: - Private -
    
    private var storedFrameKey: String {
        return String(describing: type(of: self)) + "/storedFrameKey"
    }
    
    private var storedFrame: NSRect? {
        guard let string = UserDefaults.standard.string(forKey: storedFrameKey) else {
            return nil
        }
        let rect = NSRectFromString(string)
        let newSize = NSSize(width: 80, height: 280)
        let newRect = NSRect(origin: rect.origin, size: newSize)
        return newRect
    }
    
    @objc private func windowWillClose() {
        guard let frame = window?.frame else {
            return
        }
        UserDefaults.standard.set(NSStringFromRect(frame), forKey: storedFrameKey)
    }

}
